// create your own env.local.js by this template, fill it with your values.

export default {
  auth: {
    android: {
      clientId: 'YOUR_ANDROID_CLIENT_ID',
    },
    ios: {
      clientId: 'YOUR_IOS_CLIENT_ID',
    },
    behavior: 'web', // or 'system' for standalone
  },

// use this for fake/fast "authentication" setup, but you wont be able to call APIs with this one.
// for APIs use real auth provider.
  mock: {
    type: 'success',
    user: {
      name: 'Whatever',
      photoUrl: 'https://en.gravatar.com/userimage/x',
    },
    accessToken: 'access',
    refreshToken: 'refresh',
  }
}

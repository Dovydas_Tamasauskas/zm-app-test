import styled from 'styled-components/native';

import colors from '../colors';

const MessageWrapper = styled.View`
  width: 100%;
`;

const Wrapper = styled.TouchableOpacity`
  align-items: center;
  justify-content: center;
  flex-direction: row;
  background-color: ${colors.bg};
  border-bottom-width: 2px;
  border-bottom-color: ${colors.bgElementDark};
`;

const TextWrapper = styled.View`
  flex: 1;
  padding: 10px;
  background-color: ${props => (props.danger ? colors.bgElement : colors.bgLight)};
`;

const IconWrapper = styled.View`
  padding: 8px 8px 7px 12px;
  align-items: center;
  justify-content: center;
  background-color: ${colors.bgLight};
`;

export {
  MessageWrapper,
  Wrapper,
  TextWrapper,
  IconWrapper,
};

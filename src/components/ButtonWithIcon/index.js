import React from 'react';
import PropTypes from 'prop-types';

import colors from '../colors';
import { Icon } from '../styles';
import { Wrapper, IconWrapper, TextWrapper } from './styles';

const Component = ({ icon, onPress, children, ...props }) => (
  <Wrapper onPress={onPress} {...props}>
    <IconWrapper><Icon name={icon} size={24} color={colors.bgElement} /></IconWrapper>
    <TextWrapper>{children}</TextWrapper>
  </Wrapper>
);

export default Component;

Component.propTypes = {
  icon: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
  children: PropTypes.object.isRequired,
};

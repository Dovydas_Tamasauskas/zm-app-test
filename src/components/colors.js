export default {
  bg: '#393e46',
  bgDark: '#303338',
  bgLight: '#434951',
  bgElement: '#EAA82A',
  bgElementDark: '#CA880A',
  text: '#f9faf3',
  textDark: '#a9aaa3',
};

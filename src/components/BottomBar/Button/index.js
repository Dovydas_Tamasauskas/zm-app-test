import React from 'react';
import PropTypes from 'prop-types';

import TouchableOpacity from './styles';

const Component = ({ activeTintColor, inactiveTintColor, navigation, renderIcon, index, route }) => {
  const active = navigation.state.index === index;
  const color = active ? activeTintColor : inactiveTintColor;
  return (
    <TouchableOpacity active={active} onPress={() => (!active) && navigation.navigate(route.routeName)}>
      {renderIcon({ route, tintColor: color, focused: active, index })}
    </TouchableOpacity>
  );
};

export default Component;

Component.propTypes = {
  activeTintColor: PropTypes.string.isRequired,
  inactiveTintColor: PropTypes.string.isRequired,
  renderIcon: PropTypes.func.isRequired,
  index: PropTypes.number.isRequired,
  route: PropTypes.object.isRequired,
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};

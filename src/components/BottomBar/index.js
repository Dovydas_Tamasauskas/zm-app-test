import React from 'react';
import PropTypes from 'prop-types';

import Button from './Button';
import BottomBar from './styles';

const Component = ({ navigation, ...props }) => (
  <BottomBar>
    {navigation.state.routes.map((route, idx) => (
      <Button key={idx} {...props} navigation={navigation} route={route} index={idx} />
    ))}
  </BottomBar>
);

export default Component;

Component.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};

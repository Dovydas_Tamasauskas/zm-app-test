import React from 'react';

import { TopLayer, CenterLayer, BottomLayer, SideLayer, FocusLayer, CornerContainer, Corner } from './styles';

const Component = () => (
  <>
    <TopLayer />
    <CenterLayer>
      <SideLayer />
      <FocusLayer>
        <CornerContainer>
          <Corner topBorder leftBorder />
          <Corner bottomBorder leftBorder />
        </CornerContainer>
        <CornerContainer>
          <Corner topBorder rightBorder />
          <Corner bottomBorder rightBorder />
        </CornerContainer>
      </FocusLayer>
      <SideLayer />
    </CenterLayer>
    <BottomLayer />
  </>
);

export default Component;

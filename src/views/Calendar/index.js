import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import View from './view';
import { actions } from '../../modules';

class Component extends React.Component {
  swiper = null;

  constructor(props) {
    super(props);
    this.props.dispatch(actions.calendars.load);
    this.props.navigation.addListener('willFocus', () => this.scrollToSelected());
  }

  setSwiper = (ref) => {
    this.swiper = ref;
  };

  scrollToSelected = () => {
    const roomName = this.props.navigation.getParam('code');
    if (roomName && (this.swiper !== null)) {
      const rooms = this.props.calendars.data;
      const names = Object.keys(rooms).map(key => rooms[key].summary);
      const to = names.indexOf(roomName) - this.swiper.state.index;
      this.swiper.scrollBy(to, false);
    }
  };

  render() {
    return (
      <View {...this.props} {...this.state} setSwiper={this.setSwiper} />
    );
  }
}

const mapStateToProps = state => ({
  calendars: state.calendars,
});

export default connect(mapStateToProps)(Component);

Component.propTypes = {
  calendars: PropTypes.shape({
    data: PropTypes.object,
  }).isRequired,
  dispatch: PropTypes.func.isRequired,
  navigation: PropTypes.shape({
    addListener: PropTypes.func.isRequired,
    getParam: PropTypes.func.isRequired,
  }).isRequired,
};

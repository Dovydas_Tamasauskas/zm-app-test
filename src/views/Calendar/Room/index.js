import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import View from './view';
import { actions } from '../../../modules';

class Component extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showGaps: false,
      selectedGap: null,
    };
    this.props.navigation.addListener('willFocus', () => this.switchView());
  }

  switchView = () => {
    if (this.props.data.summary === this.props.navigation.getParam('code')) {
      this.setState({ showGaps: !!this.props.navigation.getParam('showGaps') });
    }
  };

  handleSwitch = () => {
    this.setState(prevState => ({ ...prevState, showGaps: !prevState.showGaps }));
  };

  handleSelect = (data) => {
    this.setState(prevState => ({ ...prevState, selectedGap: data, showGaps: false }));
  };

  handleRefresh = () => {
    this.props.dispatch(actions.calendars.load);
  };

  getSelectedGap = () => {
    let actualGap = (this.props.data.gaps && this.state.selectedGap)
      ? this.props.data.gaps.filter(gap => gap.start === this.state.selectedGap.start)
      : [];
    actualGap = actualGap.length ? actualGap[0] : null;
    return actualGap || (this.props.data.gaps ? this.props.data.gaps[0] : null);
  };

  handlers = {
    switch: this.handleSwitch,
    select: this.handleSelect,
    refresh: this.handleRefresh,
  };

  render() {
    return (
      <View {...this.props} {...this.state} selectedGap={this.getSelectedGap()} handlers={this.handlers} />
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  data: state.events.list.data[ownProps.id],
});

export default connect(mapStateToProps)(Component);

Component.propTypes = {
  data: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
  navigation: PropTypes.shape({
    addListener: PropTypes.func.isRequired,
    getParam: PropTypes.func.isRequired,
  }).isRequired,
};

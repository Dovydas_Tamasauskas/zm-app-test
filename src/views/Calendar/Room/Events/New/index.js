import React from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import PropTypes from 'prop-types';
import { range } from 'lodash';

import config from '../../../../../config';
import { actions } from '../../../../../modules';
import View from './view';

class Component extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      start: moment(props.gap.start).format('HH:mm'),
      duration: config.durations[0].value,
    };
  }

  componentDidUpdate(prevProps) {
    if ((this.props.gap.start !== prevProps.gap.start)
      || (this.props.gap.end !== prevProps.gap.end)) {
      // eslint-disable-next-line
      this.setState(prevState => ({ ...prevState, start: moment(this.props.gap.start).format('HH:mm') }));
    }
  }

  getStarts = () => {
    const max = moment(this.props.gap.end).set({ second: 0, millisecond: 0 })
      .diff(moment(this.props.gap.start).set({ second: 0, millisecond: 0 }), 'minutes') - this.state.duration;
    return range(0, max + 1, this.state.duration)
      .map(duration => moment(this.props.gap.start).add(duration, 'minutes').format('HH:mm'));
  };

  getDurations = () => {
    let max = moment(this.props.gap.end).set({ second: 0, millisecond: 0 })
      .diff(moment(this.state.start, 'HH:mm').set({ second: 0, millisecond: 0 }), 'minutes');
    max = (max > config.maxGapDuration) ? config.maxGapDuration : max;
    return config.durations.filter(duration => (duration.value <= max));
  };

  handleChangeValue = name => (value) => {
    this.setState(prevState => ({ ...prevState, [name]: value }));
  };

  handleCreate = duration => () => {
    this.props.dispatch(actions.events.create(this.props.room, this.props.user.name, this.state.start, duration));
  };

  handlers = {
    create: this.handleCreate,
    changeValue: this.handleChangeValue,
  };

  render() {
    return (
      <View
        {...this.props}
        {...this.state}
        starts={this.getStarts()}
        durations={this.getDurations()}
        handlers={this.handlers}
      />
    );
  }
}

const mapStateToProps = state => ({
  user: state.user.data.user,
});

export default connect(mapStateToProps)(Component);

Component.propTypes = {
  user: PropTypes.object.isRequired,
  room: PropTypes.object.isRequired,
  gap: PropTypes.shape({
    start: PropTypes.string.isRequired,
    end: PropTypes.string.isRequired,
  }).isRequired,
  dispatch: PropTypes.func.isRequired,
};

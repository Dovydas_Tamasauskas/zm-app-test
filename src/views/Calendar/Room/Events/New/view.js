import React from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import { Picker } from 'native-base';
import { Platform } from 'react-native';

import colors from '../../../../../components/colors';
import { Row, Wrapper, Button, ActivityIndicator } from './styles';
import { Text } from '../../../../../components/styles';

const Component = ({ room, gap, starts, durations, start, handlers }) => (
  <Wrapper>
    {room.pending && <ActivityIndicator color={colors.bgElement} size="large" />}
    <Text size={20}>{moment(gap.start).format('YYYY-MM-DD')}</Text>
    <Text size={20}>{moment(gap.start).format('HH:mm')} - {moment(gap.end).format('HH:mm')}</Text>
    <Row style={{ width: '50%' }}>
      <Text size={16}>From:</Text>
      <Picker
        style={Platform.OS === 'ios' ? {} : { color: colors.text }}
        textStyle={{ color: colors.text }}
        selectedValue={start}
        onValueChange={handlers.changeValue('start')}
      >
        {starts.map((data, idx) => (
          <Picker.Item key={idx} value={data} label={data} />
        ))}
      </Picker>
    </Row>
    <Row>
      {durations.map(data => (
        <Button key={data.value} onPress={handlers.create(data.value)}>
          <Text>{data.label}</Text>
        </Button>
      ))}
    </Row>
  </Wrapper>
);

export default Component;

Component.propTypes = {
  room: PropTypes.shape({
    pending: PropTypes.bool,
  }).isRequired,

  gap: PropTypes.shape({
    start: PropTypes.string,
    end: PropTypes.string,
  }).isRequired,
  start: PropTypes.string.isRequired,

  starts: PropTypes.arrayOf(PropTypes.string).isRequired,
  durations: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.number.isRequired,
    label: PropTypes.string.isRequired,
  })).isRequired,

  handlers: PropTypes.shape({
    create: PropTypes.func.isRequired,
    changeValue: PropTypes.func.isRequired,
  }).isRequired,
};

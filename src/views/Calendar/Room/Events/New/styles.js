import styled from 'styled-components/native';

import { TouchableOpacity } from '../../../../../components/styles';
import colors from '../../../../../components/colors';

const Wrapper = styled.View`
  align-items: center;
  justify-content: center;
  background-color: ${colors.bgDark};
  padding: 15px;
`;

const Button = styled(TouchableOpacity)`
  width: 60px;
  padding-left: 5px;
  padding-right: 5px;
  margin: 10px;
`;

const Row = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-around;
`;

const ActivityIndicator = styled.ActivityIndicator`
  transform: scale(2);
  z-index: 1;
  width: 100%;
  height: 60%;
  background-color: rgba(50, 50, 50, .3);
  position: absolute;
`;

export {
  Wrapper,
  Button,
  Row,
  ActivityIndicator,
};

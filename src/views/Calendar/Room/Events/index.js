import React from 'react';
import { ScrollView } from 'react-native';
import PropTypes from 'prop-types';

import ExistingView from './Existing';
import NewView from './New';

const Component = ({ room, gap }) => (
  <ScrollView>
    {gap && <NewView room={room} gap={gap} />}
    <ExistingView room={room} />
  </ScrollView>
);

export default Component;

Component.propTypes = {
  room: PropTypes.object.isRequired,
  gap: PropTypes.object,
};

Component.defaultProps = {
  gap: null,
};

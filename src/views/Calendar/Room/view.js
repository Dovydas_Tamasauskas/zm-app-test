import React from 'react';
import PropTypes from 'prop-types';
import { Content, List } from 'native-base';
import { RefreshControl, ScrollView } from 'react-native';
import { View, Text } from '../../../components/styles';
import { Wrapper, FakeSwipe, Room } from './styles';
import color from '../../../components/colors';
import Gap from './Gap';
import Events from './Events';

const Component = ({ data, selectedGap, showGaps, handlers }) => (data.summary ? (
  <View style={{ justifyContent: 'flex-start' }}>
    <Wrapper style={{ zIndex: 1 }}>
      <FakeSwipe borderLeft={5} />
      <ScrollView
        refreshing={data.fetching}
        refreshControl={(
          <RefreshControl
            colors={[color.bgDark]}
            progressBackgroundColor={color.bgElement}
            onRefresh={handlers.refresh}
          />
        )}
      >
        <FakeSwipe visible borderLeft={5} borderRight={5}>
          <Room icon={showGaps ? 'calendar-plus-o' : 'calendar'} onPress={handlers.switch}>
            <Text>{data.summary.substr(0, 25)}</Text>
          </Room>
        </FakeSwipe>
      </ScrollView>
      <FakeSwipe borderRight={5} />
    </Wrapper>
    <Wrapper>
      <Content
        contentContainerStyle={{ height: '100%', paddingBottom: 180 }}
      >
        {!data.gaps || !data.gaps.length ? !data.fetching && (
          <View><Text size={18}>Empty.</Text></View>
        ) : (showGaps ? (
          <List
            style={{ width: '100%' }}
            dataArray={data.gaps}
            renderRow={gap => <Gap data={gap} onPress={() => handlers.select(gap)} />}
          />
        ) : (
          <Events room={data} gap={selectedGap} />
        ))}
      </Content>
    </Wrapper>
  </View>
) : (
  <View />
));

export default Component;

Component.propTypes = {
  data: PropTypes.object.isRequired,
  selectedGap: PropTypes.object,
  showGaps: PropTypes.bool.isRequired,
  handlers: PropTypes.shape({
    switch: PropTypes.func.isRequired,
    refresh: PropTypes.func.isRequired,
  }).isRequired,
};

Component.defaultProps = {
  selectedGap: null,
};

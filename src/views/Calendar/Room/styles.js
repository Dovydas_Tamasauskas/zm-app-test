import styled from 'styled-components/native';

import colors from '../../../components/colors';
import { ButtonWithIcon } from '../../../components';

const Wrapper = styled.View`
  align-items: center;
  justify-content: center;
  flex-direction: row;
  background-color: ${colors.bg};
`;

const FakeSwipe = styled.View`
  background-color: ${colors.bgDark};
  padding: 20px 5px 20px 5px;
  margin: ${props => (props.visible ? '10px' : 0)};
  height: 100px;
  flex: ${props => (props.visible ? 1 : 0)};
  border-bottom-right-radius: ${props => (props.borderLeft ? 5 : 0)};
  border-top-right-radius: ${props => (props.borderLeft ? 5 : 0)};
  border-bottom-left-radius: ${props => (props.borderRight ? 5 : 0)};
  border-top-left-radius: ${props => (props.borderRight ? 5 : 0)};
`;

const Room = styled(ButtonWithIcon)`
  align-self: center;
  max-width: 90%;
`;

export {
  Wrapper,
  FakeSwipe,
  Room,
};

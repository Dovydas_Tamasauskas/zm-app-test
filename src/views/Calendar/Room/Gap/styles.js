import styled from 'styled-components/native';

import colors from '../../../../components/colors';

const Wrapper = styled.TouchableOpacity`
  align-items: center;
  justify-content: center;
  flex-direction: row;
  width: 100%;
  border-bottom-right-radius: 5px;
  border-top-right-radius: 5px;
  border-bottom-left-radius: 5px;
  border-top-left-radius: 5px;
  border-bottom-width: 2px;
  border-bottom-color: ${colors.bgElementDark};
`;

const TextWrapper = styled.View`
  padding: 10px;
  align-items: center;
  flex: 1;
  border-bottom-left-radius: 5px;
  border-top-left-radius: 5px;
  background-color: ${colors.bgElement};
`;

const IconWrapper = styled.View`
  padding: 20px 35px 20px 35px;
  height: 100%;
  align-items: center;
  flex: 0;
  border-bottom-right-radius: 5px;
  border-top-right-radius: 5px;
  background-color: ${colors.bgLight};
`;

export {
  Wrapper,
  TextWrapper,
  IconWrapper,
};

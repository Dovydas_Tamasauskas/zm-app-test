import React from 'react';
import PropTypes from 'prop-types';
import { ListItem } from 'native-base';
import moment from 'moment';

import colors from '../../../../components/colors';
import { Text, Icon } from '../../../../components/styles';
import { Wrapper, TextWrapper, IconWrapper } from './styles';

const formatTime = time => moment(time).format('HH:mm');
const getDuration = (from, to) => moment.utc(moment(to).diff(moment(from))).format('HH:mm');

const Component = ({ data, onPress }) => (
  <ListItem style={{ borderBottomColor: 0 }}>
    <Wrapper onPress={onPress}>
      <TextWrapper>
        <Text size={20}>{formatTime(data.start)} - {formatTime(data.end)}</Text>
        <Text size={16}>{getDuration(data.start, data.end)}</Text>
      </TextWrapper>
      <IconWrapper><Icon name="clock-o" size={24} color={colors.bgElement} /></IconWrapper>
    </Wrapper>
  </ListItem>
);

export default Component;

Component.propTypes = {
  data: PropTypes.object.isRequired,
  onPress: PropTypes.func.isRequired,
};

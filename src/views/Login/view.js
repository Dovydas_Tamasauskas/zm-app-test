import React from 'react';
import PropTypes from 'prop-types';

import { View, Text, Image } from '../../components/styles';
import ButtonWithIcon from '../../components/ButtonWithIcon';

const Component = ({ fetching, data, handlers }) => (
  <View>
    {!fetching && (
      data
        ? (
          <View>
            <Text size={24}>{data.user && data.user.name}</Text>
            <Image source={{ uri: data.user && data.user.photoUrl }} />
            <ButtonWithIcon icon="sign-out" onPress={handlers.handleSignOut}>
              <Text>LOGOUT</Text>
            </ButtonWithIcon>
          </View>
        )
        : (
          <View>
            <ButtonWithIcon icon="google" onPress={handlers.handleSignIn}>
              <Text>SIGN IN WITH GOOGLE</Text>
            </ButtonWithIcon>
          </View>
        )
    )}
  </View>
);

export default Component;

Component.propTypes = {
  fetching: PropTypes.bool.isRequired,
  data: PropTypes.shape({
    name: PropTypes.string,
    photoUrl: PropTypes.string,
  }),
  handlers: PropTypes.shape({
    handleSignIn: PropTypes.func.isRequired,
    handleSignOut: PropTypes.func.isRequired,
  }).isRequired,
};

Component.defaultProps = {
  data: null,
};

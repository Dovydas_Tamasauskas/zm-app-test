import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import user from '../../modules/user';
import View from './view';

class Component extends React.Component {
  handleSignIn = () => {
    this.props.dispatch(user.actions.signIn(this.props.navigation));
  };

  handleSignOut = () => {
    this.props.dispatch(user.actions.signOut());
    this.props.navigation.navigate('Auth');
  };

  handlers = {
    handleSignIn: this.handleSignIn,
    handleSignOut: this.handleSignOut,
  };

  render() {
    return <View {...this.props.user} handlers={this.handlers} />;
  }
}

const mapStateToProps = state => ({
  user: state.user,
});

export default connect(mapStateToProps)(Component);

Component.propTypes = {
  user: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};

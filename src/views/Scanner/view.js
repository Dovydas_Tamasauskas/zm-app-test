import React from 'react';
import PropTypes from 'prop-types';

import Camera from './Camera';
import Result from './Result';
import { View, Text } from '../../components/styles';

const Component = ({ hasCameraPermission, handlers, ...props }) => (
  <View>
    {hasCameraPermission ? (
      <Camera handleSubmitBarcode={handlers.submitBarcode} />
    ) : (
      <Text size={18}>
        {(hasCameraPermission === null) ? 'Requesting for camera permission' : 'Camera permission is not granted'}
      </Text>
    )}
    <Result {...props} handleCancel={handlers.cancel} />
  </View>
);

Component.propTypes = {
  hasCameraPermission: PropTypes.bool,
  handlers: PropTypes.shape({
    submitBarcode: PropTypes.func,
    cancel: PropTypes.func,
  }).isRequired,
};

Component.defaultProps = {
  hasCameraPermission: undefined,
};

export default Component;

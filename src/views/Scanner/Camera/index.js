import React from 'react';
import { BarCodeScanner } from 'expo';
import PropTypes from 'prop-types';

import { Camera } from '../../../components';

const Component = ({ handleSubmitBarcode }) => (
  <BarCodeScanner onBarCodeRead={handleSubmitBarcode} style={{ marginTop: 15 }}>
    <Camera />
  </BarCodeScanner>
);

Component.propTypes = {
  handleSubmitBarcode: PropTypes.func.isRequired,
};

export default Component;

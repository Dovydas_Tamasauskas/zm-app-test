import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  bottomBarView: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'rgba(0,0,0,0.5)',
    padding: 15,
    flexDirection: 'row',
  },
  bottomBarInvalid: {
    backgroundColor: 'rgba(255,0,0,0.2)',
  },

  itemText: {
    flex: 1,
    color: '#fefefe',
    alignSelf: 'center',
  },
  itemButton: {
    flex: 0,
  },
});

export default styles;

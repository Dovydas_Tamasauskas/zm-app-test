import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { View } from 'native-base';

import { Message } from '../../components';
import messages from '../../modules/messages';

const Component = ({ info, errors, close }) => (
  <View>
    {info && <Message text={info} onPress={() => close({ info: true })} />}
    {errors.map((error, idx) => <Message key={idx} danger text={error} onPress={() => close({ idx })} />)}
  </View>
);

const mapStateToProps = state => ({
  info: state.messages.info,
  errors: state.messages.errors,
});

const mapDispatchToProps = ({
  close: messages.actions.close,
});

export default connect(mapStateToProps, mapDispatchToProps)(Component);

Component.propTypes = {
  info: PropTypes.string,
  errors: PropTypes.arrayOf(PropTypes.string).isRequired,
  close: PropTypes.func.isRequired,
};

Component.defaultProps = {
  info: null,
};

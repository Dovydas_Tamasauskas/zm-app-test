import { api } from '../../../utils';

export default {
  SIGN_IN: api.ACTION_TYPES('SIGN_IN'),
  SIGN_OUT: 'SIGN_OUT',
  UPDATE_TOKEN: api.ACTION_TYPES('UPDATE_TOKEN'),
};

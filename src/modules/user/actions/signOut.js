import TYPES from './types';
import message from '../../messages';

export const action = () => (dispatch) => {
  dispatch(message.actions.clear());
  dispatch({ type: TYPES.SIGN_OUT });
};

export default action;

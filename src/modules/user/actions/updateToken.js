import TYPES from './types';
import message from '../../messages';
import { handleError } from '../../../utils';

const action = api => refreshToken => async (dispatch) => {
  dispatch({ type: TYPES.UPDATE_TOKEN.REQUEST });
  try {
    const { data } = await api.execute(api.endpoints.getToken(refreshToken));
    dispatch({
      type: TYPES.UPDATE_TOKEN.SUCCESS,
      payload: {
        accessToken: data.access_token,
        expiresIn: data.expires_in,
        idToken: data.id_token,
      },
    });
  } catch (error) {
    dispatch({ type: TYPES.UPDATE_TOKEN.FAILURE, payload: handleError(error) });
    dispatch(message.actions.error(error));
  }
};

export default action;

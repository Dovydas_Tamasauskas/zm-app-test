import { handleError } from '../../../utils';
import TYPES from './types';
import message from '../../messages';
import events from '../../events';

const ROOM_SUFFIX = '@resource.calendar.google.com';

const action = api => async (dispatch) => {
  dispatch({ type: TYPES.ROOMS.REQUEST });
  try {
    const { data } = await api.execute(api.endpoints.getCalendarList());
    const roomCalendars = data.items
      .filter(calendar => (calendar.id.indexOf(ROOM_SUFFIX) >= 0))
      .map(room => ({ [room.id]: room }));
    let rooms = {};
    if (roomCalendars.length) {
      rooms = Object.assign(...roomCalendars);
      Object.keys(rooms).forEach(key => dispatch(events.actions.load(key)));
    } else {
      dispatch(message.actions.error('User has no rooms assigned.'));
    }
    dispatch({ type: TYPES.ROOMS.SUCCESS, payload: rooms });
  } catch (error) {
    dispatch({ type: TYPES.ROOMS.FAILURE, payload: handleError(error) });
    dispatch(message.actions.error(error));
  }
};

export default action;

import moment from 'moment';
import action from './load';
import { getDefaultFrom } from './load';
import TYPES from './types';
import config from '../../../config';

const calendarsListResponse = {
  data: {
    items: [
      {
        id: 'domain_longId_0@group.calendar.google.com',
        summary: 'Name_0 Room',
      }, {
        id: 'domain_longId_1@resource.calendar.google.com',
        summary: 'Name_1 Room',
      }, {
        id: 'domain_longId_2@resource.calendar.google.com',
        summary: 'Name_2 Room',
      },
    ],
  },
};

const busyResponse = {
  data: {
    calendars: {
      'domain_longId_1@resource.calendar.google.com': {
        busy: [
          {
            start: moment({ hour: 14, minute: 0 }).toISOString(),
            end: moment({ hour: 14, minute: 30 }).toISOString(),
          },
        ],
      },
      'domain_longId_2@resource.calendar.google.com': {
        busy: [
          {
            start: moment({ hour: 14, minute: 0 }).toISOString(),
            end: moment({ hour: 15, minute: 0 }).toISOString(),
          },
          {
            start: moment({ hour: 16, minute: 0 }).toISOString(),
            end: moment({ hour: 17, minute: 0 }).toISOString(),
          },
        ],
      },
    },
  },
};

const calOne = {
  firstBusy: {
    start: busyResponse.data.calendars['domain_longId_1@resource.calendar.google.com'].busy[0].start,
    end: busyResponse.data.calendars['domain_longId_1@resource.calendar.google.com'].busy[0].end,
  },
};
const calTwo = {
  firstBusy: {
    start: busyResponse.data.calendars['domain_longId_2@resource.calendar.google.com'].busy[0].start,
    end: busyResponse.data.calendars['domain_longId_2@resource.calendar.google.com'].busy[0].end,
  },
  secondBusy: {
    start: busyResponse.data.calendars['domain_longId_2@resource.calendar.google.com'].busy[1].start,
    end: busyResponse.data.calendars['domain_longId_2@resource.calendar.google.com'].busy[1].end,
  },
};

const calculatedFree = {
  calOne:
    moment(getDefaultFrom())
      .isBefore(calOne.firstBusy.start)
      ? [
        { start: getDefaultFrom(), end: calOne.firstBusy.start },
        { start: calOne.firstBusy.end, end: moment(config.maxTime, 'HH:mm').toISOString() },
      ]
      : [{ start: calOne.firstBusy.end, end: moment(config.maxTime, 'HH:mm').toISOString() }],
  calTwo:
    moment(getDefaultFrom())
      .isBefore(calTwo.firstBusy.start)
      ? [
        { start: getDefaultFrom(), end: calTwo.firstBusy.start },
        { start: calTwo.firstBusy.end, end: calTwo.secondBusy.start },
        { start: calTwo.secondBusy.end, end: moment(config.maxTime, 'HH:mm').toISOString() },
      ]
      : [
        { start: calTwo.firstBusy.end, end: calTwo.secondBusy.start },
        { start: calTwo.secondBusy.end, end: moment(config.maxTime, 'HH:mm').toISOString() },
      ],
};

const passedGaps = gap => moment(gap.end).isAfter(moment(getDefaultFrom()));

const result = [{
  busy: [{
    start: calOne.firstBusy.start,
    end: calOne.firstBusy.end,
  }],
  free: calculatedFree.calOne.filter(passedGaps),
  room: {
    id: 'domain_longId_1@resource.calendar.google.com',
    summary: 'Name_1 Room',
  },
}, {
  busy: [
    { start: calTwo.firstBusy.start, end: calTwo.firstBusy.end },
    { start: calTwo.secondBusy.start, end: calTwo.secondBusy.end },
  ],
  free: calculatedFree.calTwo.filter(passedGaps),
  room: {
    id: 'domain_longId_2@resource.calendar.google.com',
    summary: 'Name_2 Room',
  },
}];

const apiSuccess = {
  execute: f => f,
  endpoints: {
    getBusy: () => busyResponse,
    getCalendarList: () => calendarsListResponse,
  },
};

const apiFailure = {
  execute: f => f,
  endpoints: {
    getBusy: () => {
      throw new Error('Bad request');
    },
    getCalendarList: () => {
      throw new Error('Bad request');
    },
  },
};


describe('Calendars action creator', () => {
  it('should dispatch CALENDARS.SUCCESS action', async () => {
    const dispatch = jest.fn();
    await action(apiSuccess)(dispatch);
    expect(dispatch)
      .toHaveBeenNthCalledWith(
        1,
        { type: TYPES.REQUEST },
      );
    expect(dispatch)
      .toHaveBeenNthCalledWith(
        2,
        {
          type: TYPES.SUCCESS,
          payload: result,
        },
      );
  });

  it('should dispatch CALENDARS.FAILURE action', async () => {
    const dispatch = jest.fn();
    await action(apiFailure)(dispatch);
    expect(dispatch)
      .toHaveBeenNthCalledWith(
        1,
        { type: TYPES.REQUEST },
      );
    expect(dispatch)
      .toHaveBeenNthCalledWith(
        2,
        {
          type: TYPES.FAILURE,
          payload: 'Bad request',
        },
      );
  });
});

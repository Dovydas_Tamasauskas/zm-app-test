import { api } from '../../../utils';

export default {
  LOAD: api.ACTION_TYPES('LOAD_EVENTS'),
  CREATE: api.ACTION_TYPES('CREATE_EVENT'),
  DELETE: api.ACTION_TYPES('DELETE_EVENT'),
};

import moment from 'moment';
import TYPES from './types';
import messages from '../../messages';

const action = api => (room, user, start, duration) => async (dispatch) => {
  dispatch({ type: TYPES.CREATE.REQUEST, payload: { roomId: room.id } });
  const startTime = moment(moment().format('YYYY-MM-DDT') + start).toISOString();
  const endTime = moment(startTime).add(duration, 'minutes');

  try {
    const requestBody = {
      end: {
        dateTime: endTime,
      },
      start: {
        dateTime: startTime,
      },
      creator: {
        displayName: user,
      },
      summary: `${user} reservation`,
      attendees: [
        {
          email: room.id,
        },
      ],
    };

    const { data } = await api.execute(api.endpoints.createEvent(), requestBody);
    dispatch({ type: TYPES.CREATE.SUCCESS, payload: { roomId: room.id, event: data } });
    dispatch(messages.actions.info('Event created.'));
  } catch (err) {
    dispatch({ type: TYPES.CREATE.FAILURE, payload: { roomId: room.id, error: err } });
    dispatch(messages.actions.error(err));
  }
};

export default action;

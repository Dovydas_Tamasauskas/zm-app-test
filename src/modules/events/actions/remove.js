import TYPES from './types';
import messages from '../../messages';

const action = api => (roomId, id) => async (dispatch) => {
  dispatch({ type: TYPES.DELETE.REQUEST, payload: { roomId } });
  try {
    await api.execute(api.endpoints.deleteEvent(id));
    dispatch({ type: TYPES.DELETE.SUCCESS, payload: { roomId, eventId: id } });
    dispatch(messages.actions.info('Event deleted.'));
  } catch (err) {
    if (err instanceof Object && err.response.status === 410) {
      dispatch({ type: TYPES.DELETE.SUCCESS, payload: { roomId, eventId: id } });
    } else {
      dispatch({ type: TYPES.DELETE.FAILURE, payload: { roomId, error: err } });
    }
  }
};

export default action;

import TYPES from './types';
import load from './load';
import create from './create';
import remove from './remove';
import { api } from '../../../utils';

export { TYPES };

export default {
  load: load(api),
  create: create(api),
  remove: remove(api),
};

import action from './create';
import TYPES from './types';

let dispatch;

const apiSuccess = {
  execute: () => ({ data: {
    id: 123,
  } }),
  endpoints: {
    createEvent: () => {},
  },
};

const apiFailure = {
  execute: () => { throw new Error('Bad request'); },
  endpoints: {
    createEvent: () => {},
  },
};

describe('Event creation action creator', () => {
  beforeEach(() => {
    dispatch = jest.fn();
  });

  it('should dispatch CREATE.SUCCESS action', async () => {
    await action(apiSuccess)(
      { id: '12ew54we77' },
      'Name LastName',
      '15:00',
      15,
    )(dispatch);
    expect(dispatch).toHaveBeenNthCalledWith(
      1,
      { type: TYPES.CREATE.REQUEST, payload: { roomId: '12ew54we77' } },
    );
    expect(dispatch).toHaveBeenNthCalledWith(
      2,
      { type: TYPES.CREATE.SUCCESS,
        payload: { event: { id: 123 }, roomId: '12ew54we77' } },
    );
  });

  it('should dispatch CREATE.FAILURE action', async () => {
    await action(apiFailure)(
      { id: '12ew54we77' },
      'Name LastName',
      '15:00',
      15,
    )(dispatch);
    expect(dispatch).toHaveBeenNthCalledWith(
      1,
      { type: TYPES.CREATE.REQUEST, payload: { roomId: '12ew54we77' } },
    );
    expect(dispatch).toHaveBeenNthCalledWith(
      2,
      { type: TYPES.CREATE.FAILURE, payload: { error: Error('Bad request'), roomId: '12ew54we77' } },
    );
  });
});

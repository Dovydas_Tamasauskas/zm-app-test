import { handleError } from '../../../utils';
import TYPES from './types';
import message from '../../messages';

const action = api => roomId => async (dispatch) => {
  dispatch({ type: TYPES.LOAD.REQUEST, payload: { id: roomId } });
  try {
    const { data } = await api.execute(api.endpoints.getCalendarEvents(roomId));
    dispatch({ type: TYPES.LOAD.SUCCESS, payload: { id: roomId, ...data } });
  } catch (error) {
    dispatch({ type: TYPES.LOAD.FAILURE, payload: { id: roomId, error: handleError(error) } });
    dispatch(message.actions.error(error));
  }
};

export default action;

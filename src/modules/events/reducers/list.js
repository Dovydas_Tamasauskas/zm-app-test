import moment from 'moment';

import { TYPES } from '../actions';
import getGaps from './getGaps';

const initialState = {
  data: {},
};

const actions = ({
  [TYPES.LOAD.REQUEST]: (state, payload) => ({
    data: {
      ...state.data,
      [payload.id]: {
        ...state.data[payload.id],
        pending: true,
        error: null,
      },
    },
  }),
  [TYPES.LOAD.SUCCESS]: (state, payload) => ({
    data: {
      ...state.data,
      [payload.id]: {
        ...payload,
        gaps: getGaps(payload),
        pending: false,
        error: null,
      },
    },
  }),
  [TYPES.LOAD.FAILURE]: (state, payload) => ({
    data: {
      ...state.data,
      [payload.id]: {
        ...state.data[payload.id],
        pending: false,
        error: payload.error,
      },
    },
  }),

  [TYPES.CREATE.REQUEST]: (state, payload) => ({
    data: {
      ...state.data,
      [payload.roomId]: {
        ...state.data[payload.roomId],
        pending: true,
        error: null,
      },
    },
  }),
  [TYPES.CREATE.SUCCESS]: (state, payload) => {
    const room = { ...state.data[payload.roomId] };
    room.items = [...room.items, payload.event];
    room.items.sort((a, b) => moment(a.start.dateTime).diff(b.start.dateTime));
    return ({
      data: {
        ...state.data,
        [payload.roomId]: {
          ...room,
          gaps: getGaps(room),
          pending: false,
        },
      },
    });
  },
  [TYPES.CREATE.FAILURE]: (state, payload) => ({
    data: {
      ...state.data,
      [payload.roomId]: {
        ...state.data[payload.roomId],
        pending: false,
        error: payload.error,
      },
    },
  }),

  [TYPES.DELETE.REQUEST]: (state, payload) => ({
    data: {
      ...state.data,
      [payload.roomId]: {
        ...state.data[payload.roomId],
        pending: true,
        error: null,
      },
    },
  }),
  [TYPES.DELETE.SUCCESS]: (state, payload) => {
    const room = { ...state.data[payload.roomId] };
    room.items = [...room.items.filter(event => event.id !== payload.eventId)];
    room.items.sort((a, b) => moment(a.start.dateTime).diff(b.start.dateTime));
    return ({
      data: {
        ...state.data,
        [payload.roomId]: {
          ...room,
          gaps: getGaps(room),
          pending: false,
        },
      },
    });
  },
  [TYPES.DELETE.FAILURE]: (state, payload) => ({
    data: {
      ...state.data,
      [payload.roomId]: {
        ...state.data[payload.roomId],
        pending: false,
        error: payload.error,
      },
    },
  }),
});

const reducer = (state = initialState, action) => (
  actions[action.type] ? actions[action.type](state, action.payload) : state
);

export default reducer;

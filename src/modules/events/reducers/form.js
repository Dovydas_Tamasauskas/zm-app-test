import { TYPES } from '../actions';

const initialState = {
  pending: false,
  error: null,
};

const actions = ({
  [TYPES.CREATE.REQUEST]: state => ({
    ...state,
    pending: true,
    error: null,
  }),
  [TYPES.CREATE.SUCCESS]: () => ({
    pending: false,
    error: null,
  }),
  [TYPES.CREATE.FAILURE]: (state, payload) => ({
    ...state,
    pending: false,
    error: payload,
  }),
});

const reducer = (state = initialState, action) => (
  actions[action.type] ? actions[action.type](state, action.payload) : state
);

export default reducer;

import moment from 'moment';
import reducer from './list';
import { TYPES } from '../index';
import getGaps from './getGaps';

describe('List reducer', () => {
  const initialState = {
    data: {
      'domain_numberId_0@resource.calendar.google.com': {
        items: [],
      },
    },
  };

  it('should add new event based on payload on SUCCESS action type', async () => {
    const payload = {
      roomId: 'domain_numberId_0@resource.calendar.google.com',
      event: {
        start: {
          dateTime: moment().set({ hours: 8, minutes: 0, seconds: 0, milliseconds: 0 }),
        },
        end: {
          dateTime: moment().set({ hours: 9, minutes: 0, seconds: 0, milliseconds: 0 }),
        },
      },
    };
    const state = reducer(initialState, { type: TYPES.CREATE.SUCCESS, payload });
    expect(state).toEqual({
      data: {
        [payload.roomId]: {
          gaps: getGaps(state.data[payload.roomId]),
          items: [
            payload.event,
          ],
          pending: false,
        },
      },
    });
  });

  it('should keep past events after adding a new event', async () => {
    const payload = {
      roomId: 'domain_numberId_0@resource.calendar.google.com',
      event: {
        start: {
          dateTime: '2019-01-01T08:00:00Z',
        },
        end: {
          dateTime: '2019-01-01T09:00:00Z',
        },
      },
    };
    let state = reducer(initialState, { type: TYPES.CREATE.SUCCESS, payload });
    state = reducer(state, { type: TYPES.CREATE.SUCCESS, payload });
    state = reducer(state, { type: TYPES.CREATE.SUCCESS, payload });
    state = reducer(state, { type: TYPES.CREATE.SUCCESS, payload });

    expect(state).toEqual({
      data: {
        [payload.roomId]: {
          gaps: getGaps(state.data[payload.roomId]),
          items: [
            payload.event,
            payload.event,
            payload.event,
            payload.event,
          ],
          pending: false,
        },
      },
    });
  });

  it('should set pending to true on REQUEST action type', async () => {
    const payload = {
      roomId: 'domain_numberId_0@resource.calendar.google.com',
      event: {
        start: {
          dateTime: '2019-01-01T08:00:00Z',
        },
        end: {
          dateTime: '2019-01-01T09:00:00Z',
        },
      },
    };
    expect(reducer(initialState, { type: TYPES.DELETE.REQUEST, payload })).toEqual({
      data: {
        'domain_numberId_0@resource.calendar.google.com': {
          items: [],
          pending: true,
          error: null,
        },
      },
    });
  });

  it('should remove payload item from state on SUCCESS action type', async () => {
    const payload = {
      roomId: 'domain_numberId_0@resource.calendar.google.com',
      event: {
        start: {
          dateTime: '2019-01-01T08:00:00Z',
        },
        end: {
          dateTime: '2019-01-01T09:00:00Z',
        },
      },
    };
    const state = reducer(initialState, { type: TYPES.CREATE.SUCCESS, payload });
    expect(reducer(state, { type: TYPES.DELETE.SUCCESS, payload })).toEqual({
      data: {
        'domain_numberId_0@resource.calendar.google.com': {
          pending: false,
          items: [],
          gaps: getGaps(initialState.data[payload.roomId]),
        },
      },
    });
  });

  it('should add error specified in payload on ERROR action type', async () => {
    const payload = {
      roomId: 'domain_numberId_0@resource.calendar.google.com',
      error: 'Bad request',
    };
    expect(reducer(initialState, { type: TYPES.DELETE.FAILURE, payload })).toEqual({
      data: {
        'domain_numberId_0@resource.calendar.google.com': {
          items: [],
          pending: false,
          error: payload.error,
        },
      },
    });
  });

  it('should set save error, on event load failure', async () => {
    const payload = {
      id: 'domain_numberId_0@resource.calendar.google.com',
      error: '400 Bad request',
    };

    expect(reducer(undefined, { type: TYPES.LOAD.FAILURE, payload })).toEqual({
      data: {
        'domain_numberId_0@resource.calendar.google.com': {
          pending: false,
          error: payload.error,
        },
      },
    });
  });

  it('should set pending to true, on event load request', async () => {
    const payload = {
      id: 'domain_numberId_0@resource.calendar.google.com',
    };

    expect(reducer(undefined, { type: TYPES.LOAD.REQUEST, payload })).toEqual({
      data: {
        'domain_numberId_0@resource.calendar.google.com': {
          pending: true,
          error: null,
        },
      },
    });
  });

  it('should add events and gaps to calendar object on success', async () => {
    const payload = {
      id: 'domain_numberId_0@resource.calendar.google.com',
      items: [
        {
          start: {
            dateTime: moment().set({ hours: 8, minutes: 0, seconds: 0, milliseconds: 0 }),
          },
          end: {
            dateTime: moment().set({ hours: 9, minutes: 0, seconds: 0, milliseconds: 0 }),
          },
        },
      ],
    };
    const state = reducer(initialState, { type: TYPES.LOAD.SUCCESS, payload });
    expect(state).toEqual({
      data: {
        'domain_numberId_0@resource.calendar.google.com': {
          id: 'domain_numberId_0@resource.calendar.google.com',
          items: payload.items,
          gaps: getGaps(state.data['domain_numberId_0@resource.calendar.google.com']),
          pending: false,
          error: null,
        },
      },
    });
  });
});

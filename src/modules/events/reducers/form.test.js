import reducer from './form';
import { TYPES } from '../index';

describe('Events reducer', () => {
  it('should set pending to true on REQUEST action type', async () => {
    expect(reducer(undefined, { type: TYPES.CREATE.REQUEST })).toEqual({
      pending: true,
      error: null,
    });
  });

  it('should set pending to false on SUCCESS action type', async () => {
    expect(reducer(undefined, { type: TYPES.CREATE.SUCCESS })).toEqual({
      pending: false,
      error: null,
    });
  });

  it('should add error specified in payload to errors on ERROR action type', async () => {
    const payload = 'Bad request';
    expect(reducer(undefined, { type: TYPES.CREATE.FAILURE, payload })).toEqual({
      pending: false,
      error: payload,
    });
  });
});

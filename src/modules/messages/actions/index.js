import TYPES from './types';
import info from './info';
import error from './error';
import close from './close';
import clear from './clear';

export { TYPES };

export default {
  info,
  error,
  close,
  clear,
};

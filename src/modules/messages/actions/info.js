import TYPES from './types';

const action = message => (dispatch) => {
  dispatch({
    type: TYPES.INFO,
    payload: message,
  });
};

export default action;

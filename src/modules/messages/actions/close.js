import TYPES from './types';

const action = ({ idx = 0, info = false }) => (dispatch) => {
  dispatch({
    type: info ? TYPES.REMOVE_INFO : TYPES.REMOVE_ERROR,
    payload: idx,
  });
};

export default action;

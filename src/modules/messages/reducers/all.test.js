import reducer from './all';
import { TYPES } from '../actions';

describe('Messages reducer', () => {
  it('should set info to specified payload on INFO action type', async () => {
    const payload = 'test';
    expect(reducer(undefined, { type: TYPES.INFO, payload })).toEqual({
      info: payload,
      errors: [],
    });
  });

  it('should add error specified in payload to errors array on ERROR action type', async () => {
    const payload = 'test';
    expect(reducer(undefined, { type: TYPES.ERROR, payload })).toEqual({
      info: null,
      errors: ['test'],
    });
  });

  it('should keep only 3 last errors in errors array', async () => {
    let state = reducer(undefined, { type: TYPES.ERROR, payload: 'test1' });
    state = reducer(state, { type: TYPES.ERROR, payload: 'test2' });
    state = reducer(state, { type: TYPES.ERROR, payload: 'test3' });
    state = reducer(state, { type: TYPES.ERROR, payload: 'test4' });
    state = reducer(state, { type: TYPES.ERROR, payload: 'test5' });
    expect(state).toEqual({
      info: null,
      errors: ['test3', 'test4', 'test5'],
    });
  });

  it('should clear info on REMOVE_INFO action type', async () => {
    const state = reducer(undefined, { type: TYPES.INFO, payload: 'test' });
    expect(reducer(state, { type: TYPES.REMOVE_INFO })).toEqual({
      info: null,
      errors: [],
    });
  });

  it('should remove specified error by index on REMOVE_ERROR action type', async () => {
    let state = reducer(undefined, { type: TYPES.ERROR, payload: 'test1' });
    state = reducer(state, { type: TYPES.ERROR, payload: 'test2' });
    state = reducer(state, { type: TYPES.ERROR, payload: 'test3' });
    expect(reducer(state, { type: TYPES.REMOVE_ERROR, payload: 1 })).toEqual({
      info: null,
      errors: ['test1', 'test3'],
    });
  });
});

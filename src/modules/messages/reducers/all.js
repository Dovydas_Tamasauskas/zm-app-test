import { TYPES } from '../actions';

const initialState = {
  info: null,
  errors: [],
};

const actions = ({
  [TYPES.INFO]: (state, payload) => ({
    ...state,
    info: payload,
  }),
  [TYPES.ERROR]: (state, payload) => {
    if (state.errors.includes(payload)) {
      return ({
        ...state,
        errors: [...state.errors],
      });
    }
    return ({
      ...state,
      errors: [...state.errors, payload].slice(-3),
    });
  },

  [TYPES.REMOVE_INFO]: state => ({
    ...state,
    info: null,
  }),
  [TYPES.REMOVE_ERROR]: (state, payload) => ({
    ...state,
    errors: [...state.errors].filter((value, idx) => (idx !== payload)),
  }),

  [TYPES.CLEAR]: () => initialState,
});

const reducer = (state = initialState, action) => (
  actions[action.type] ? actions[action.type](state, action.payload) : state
);

export default reducer;

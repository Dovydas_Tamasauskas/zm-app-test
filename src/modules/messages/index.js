import actions, { TYPES } from './actions';
import reducers from './reducers';

export { TYPES };

export default {
  actions,
  reducers,
};

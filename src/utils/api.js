import axios from 'axios';
import { AsyncStorage, Platform } from 'react-native';
import URI from 'urijs';
import moment from 'moment';
import config, { security } from '../config';

const ACTION_TYPES = namespace => ({
  REQUEST: `${namespace}_REQUEST`,
  SUCCESS: `${namespace}_SUCCESS`,
  FAILURE: `${namespace}_FAILURE`,
});

const API_ROOT = 'https://www.googleapis.com/calendar/v3';

const endpoints = {
  getBusy: () => ({
    URL: `${API_ROOT}/freeBusy`,
    METHOD: 'POST',
  }),
  createEvent: () => ({
    URL: `${API_ROOT}/calendars/primary/events`,
    METHOD: 'POST',
  }),
  deleteEvent: id => ({
    URL: `${API_ROOT}/calendars/primary/events/${id}`,
    METHOD: 'DELETE',
  }),
  getCalendarList: () => ({
    URL: `${API_ROOT}/users/me/calendarList?showDeleted=false`,
    METHOD: 'GET',
  }),
  getCalendarEvents: (id, timeMin, timeMax) => ({
    URL: new URI(`${API_ROOT}/calendars/${id}/events`).query({
      timeMin: timeMin || moment().toISOString(),
      timeMax: timeMax || moment(config.maxTime, 'HH:mm').toISOString(),
      showDeleted: false,
      singleEvents: true,
      orderBy: 'startTime',
    }).toString(),
    METHOD: 'GET',
  }),
  getToken: refreshToken => ({
    URL: new URI('https://www.googleapis.com/oauth2/v4/token').query({
      client_id: security.auth[Platform.OS] && security.auth[Platform.OS].clientId,
      refresh_token: refreshToken,
      grant_type: 'refresh_token',
    }).toString(),
    METHOD: 'POST',
  }),
};

const getUser = async () => {
  let { user } = JSON.parse(await AsyncStorage.getItem('persist:root'));
  let userObj = JSON.parse(user);
  while (userObj.fetching) {
    // eslint-disable-next-line
    user = JSON.parse(await AsyncStorage.getItem('persist:root')).user;
    userObj = JSON.parse(user);
  }
  return userObj;
};

const getHeaders = async () => {
  const { data } = await getUser();
  if (!data) {
    throw new Error('User is not authenticated.');
  }

  // TODO: validate token before hitting backend? expiration, etc.?
  return ({
    Authorization: `Bearer ${data.accessToken}`,
  });
};

const execute = async (api, data) => axios({
  url: api.URL,
  data,
  method: api.METHOD,
  headers: await getHeaders(),
});

export default {
  ACTION_TYPES,
  execute,
  endpoints,
};

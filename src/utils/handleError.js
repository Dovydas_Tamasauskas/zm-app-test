export default (error) => {
  console.log(error);
  if (error instanceof Object) {
    if (error.response) {
      console.log(error.response.data);
      return error.response.data.error ? error.response.data.error.message : error.response.data.message;
    }
    return error.message;
  }
  return error;
};

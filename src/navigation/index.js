import { createSwitchNavigator, createStackNavigator, createAppContainer } from 'react-navigation';

import Bottom from './Bottom';
import screens from '../screens';
import Loading from './Loading';

const AppStack = createStackNavigator(
  { Bottom },
  { headerMode: 'none' },
);

const AuthStack = createStackNavigator(
  {
    Login: {
      screen: screens.Login,
    },
  },
  { headerMode: 'none' },
);

export default createAppContainer(createSwitchNavigator({
  Loading,
  App: AppStack,
  Auth: AuthStack,
}, {
  initialRouteName: 'Loading',
  headerMode: 'none',
}));

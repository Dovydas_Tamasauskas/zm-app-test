import React from 'react';
import { createBottomTabNavigator } from 'react-navigation';
import { Icon } from 'expo';

import { BottomBar } from '../components';
import colors from '../components/colors';
import screens from '../screens';

const bottomTabNavigator = createBottomTabNavigator({
  Calendar: {
    screen: screens.Calendar,
    navigationOptions: () => ({
      // eslint-disable-next-line
      tabBarIcon: ({ tintColor }) => (
        <Icon.MaterialCommunityIcons name="calendar-clock" size={30} color={tintColor} />
      ),
    }),
  },
  Scanner: {
    screen: screens.Scanner,
    navigationOptions: () => ({
      // eslint-disable-next-line
      tabBarIcon: ({ tintColor }) => (
        <Icon.MaterialCommunityIcons name="qrcode-scan" size={30} color={tintColor} />
      ),
    }),
  },
  Logout: {
    screen: screens.Login,
    navigationOptions: () => ({
      // eslint-disable-next-line
      tabBarIcon: ({ tintColor }) => (
        <Icon.MaterialCommunityIcons name="logout" size={30} color={tintColor} />
      ),
    }),
  },
}, {
  initialRouteName: 'Calendar',
  tabBarComponent: BottomBar,
  tabBarOptions: {
    showLabel: false,
    activeTintColor: colors.bgElement,
    inactiveTintColor: colors.text,
  },
});

export default bottomTabNavigator;
